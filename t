[1mdiff --git a/.eslintrc b/.eslintrc[m
[1mindex 87ec0d8..e859ec5 100644[m
[1m--- a/.eslintrc[m
[1m+++ b/.eslintrc[m
[36m@@ -7,6 +7,6 @@[m
     "rules": {[m
         "linebreak-style": 0,[m
         "semi": ["error", "always"],[m
[31m-        "quotes": ["error", "double"][m
[32m+[m[32m        "quotes":[2, "single", { "avoidEscape": true }][m
     }[m
 }[m
[1mdiff --git a/package-lock.json b/package-lock.json[m
[1mindex 5a13e2b..1a6933b 100644[m
[1m--- a/package-lock.json[m
[1m+++ b/package-lock.json[m
[36m@@ -1781,7 +1781,8 @@[m
         "ansi-regex": {[m
           "version": "2.1.1",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "aproba": {[m
           "version": "1.2.0",[m
[36m@@ -1802,12 +1803,14 @@[m
         "balanced-match": {[m
           "version": "1.0.0",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "brace-expansion": {[m
           "version": "1.1.11",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "balanced-match": "^1.0.0",[m
             "concat-map": "0.0.1"[m
[36m@@ -1822,17 +1825,20 @@[m
         "code-point-at": {[m
           "version": "1.1.0",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "concat-map": {[m
           "version": "0.0.1",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "console-control-strings": {[m
           "version": "1.1.0",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "core-util-is": {[m
           "version": "1.0.2",[m
[36m@@ -1949,7 +1955,8 @@[m
         "inherits": {[m
           "version": "2.0.3",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "ini": {[m
           "version": "1.3.5",[m
[36m@@ -1961,6 +1968,7 @@[m
           "version": "1.0.0",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "number-is-nan": "^1.0.0"[m
           }[m
[36m@@ -1975,6 +1983,7 @@[m
           "version": "3.0.4",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "brace-expansion": "^1.1.7"[m
           }[m
[36m@@ -1982,12 +1991,14 @@[m
         "minimist": {[m
           "version": "0.0.8",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "minipass": {[m
           "version": "2.3.5",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "safe-buffer": "^5.1.2",[m
             "yallist": "^3.0.0"[m
[36m@@ -2006,6 +2017,7 @@[m
           "version": "0.5.1",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "minimist": "0.0.8"[m
           }[m
[36m@@ -2086,7 +2098,8 @@[m
         "number-is-nan": {[m
           "version": "1.0.1",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "object-assign": {[m
           "version": "4.1.1",[m
[36m@@ -2098,6 +2111,7 @@[m
           "version": "1.4.0",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "wrappy": "1"[m
           }[m
[36m@@ -2183,7 +2197,8 @@[m
         "safe-buffer": {[m
           "version": "5.1.2",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "safer-buffer": {[m
           "version": "2.1.2",[m
[36m@@ -2219,6 +2234,7 @@[m
           "version": "1.0.2",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "code-point-at": "^1.0.0",[m
             "is-fullwidth-code-point": "^1.0.0",[m
[36m@@ -2238,6 +2254,7 @@[m
           "version": "3.0.1",[m
           "bundled": true,[m
           "dev": true,[m
[32m+[m[32m          "optional": true,[m
           "requires": {[m
             "ansi-regex": "^2.0.0"[m
           }[m
[36m@@ -2281,12 +2298,14 @@[m
         "wrappy": {[m
           "version": "1.0.2",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         },[m
         "yallist": {[m
           "version": "3.0.3",[m
           "bundled": true,[m
[31m-          "dev": true[m
[32m+[m[32m          "dev": true,[m
[32m+[m[32m          "optional": true[m
         }[m
       }[m
     },[m
